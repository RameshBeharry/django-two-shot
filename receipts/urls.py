from django.urls import path
from receipts.views import receipts_list, my_receipts

urlpatterns = [
    path("", receipts_list, name="home"),
    path("mine/", my_receipts, name="my_receipts"),

]
