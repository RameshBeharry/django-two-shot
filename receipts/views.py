from django.shortcuts import render
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required
from django.http import Http404
#from django.shortcuts import redirect, get_object_or_404

# Create your views here.

@login_required
def receipts_list(request):
    user = request.user
    receipts = Receipt.objects.all()
    context = {"receipts_list": receipts}
    return render(request, "receipts/list.html", context)

@login_required
def my_receipts(request):
    user = request.user
    receipts_auth = Receipt.objects.filter(purchaser=user)
    context = {"my_receipts": receipts_auth}
    return render(request, "reciepts/my_receipts.html", context)



    # if user.is_superuser:
    #     receipts = Receipt.objects.all()
    # else:
    #     receipts = Receipt.objects.filter(purchaser=user)
    # if not user.is_superuser:
    #     try:
    #         Receipt.objects.get(purchaser=user, id=request.GET.get('id'))
    #     except Receipt.DoesNotExist:
    #         raise Http404("Get outta here, you don't have access to this!")

# def receipts_redirect(request):
#     receipts_redirect = "/"
#     return redirect(receipts_redirect)


# def receipts_404_redirect(request):
#     receipts_redirect = get_object_or_404(Receipt)
#     context = {"receipts_redirect": receipts_redirect}
#     return render(request, "receipts/list.html", context)
