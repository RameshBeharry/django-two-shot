from django.shortcuts import render,redirect
from accounts.forms import AccountLoginForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt


# Create your views here.

def account_login(request):
    login_view = "accounts/login.html"
    context = {}

    if request.method =="POST":

        form = AccountLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(
                request,
                username=username,
                password=password,
            )

            if user is not None:
                login(request, user)
                return redirect("receipts/my_receipts.html")
    else:
        form = AccountLoginForm()

        context = {
            "form": form,
        }

    return render(request, "home", context)

def account_logout(request):
    logout(redirect)
    return redirect("login")
