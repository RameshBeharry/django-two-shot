from django import forms


class AccountLoginForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
